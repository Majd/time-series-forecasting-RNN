# Time Series Forecasting with Recurrent Neural Networks 

## Introduction

This tutorial tries to predict the future weather of a city using weather-data from several other cities.

We will be working with sequences of arbitrary length, we will use a Recurrent Neural Network (RNN).


## Location

We will use weather-data from the period 1980-2018 for five cities.

## Flowchart

We are trying to predict the weather for 24 hours into the future, given the current and past weather-data from 5 cities .

We use a Recurrent Neural Network (RNN) because it can work on sequences of arbitrary length. During training we will use sub-sequences of 1344 data-points (8 weeks) from the training-set, with each data-point or observation having 7 input-signals for the temperature for each of the 5 cities. We then want to train the neural network so it output the signal for tomorrow's temperature.

![Flowchart](https://i.imgur.com/EFjjHKc.png)

For detailed instructions please refer to the notebook.

## Finally
If you have any questions don't hesitate to contact me.

Good Read.